//
//  APIConstants.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation


enum APIConstants {
    static let defaultHeaders = ["App": "ios-joo-game", "Content-Type": "application/json"]
    static let defaultSampleData = Data()
}
