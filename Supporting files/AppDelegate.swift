//
//  AppDelegate.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var mainCoordinator: MainSceneCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navController = UINavigationController()
        mainCoordinator = MainSceneCoordinator(navigationController: navController)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navController
        mainCoordinator.runSceneFlow()
        self.window?.makeKeyAndVisible()
        return true
    }

}

