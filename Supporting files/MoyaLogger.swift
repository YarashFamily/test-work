//
//  MoyaLogger.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation
import Moya


private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}

let networkLoggerPlugin = [NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(formatter: NetworkLoggerPlugin.Configuration.Formatter(), logOptions: NetworkLoggerPlugin.Configuration.LogOptions(arrayLiteral: .verbose)))]
