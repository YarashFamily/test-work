//
//  CardViewCell.swift
//  TestApp
//
//  Created by Andrei on 25.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import Nuke


class CardViewCell: UICollectionViewCell {
    
    static let idCell: String = "CardViewCell"
    
    fileprivate let cardImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    fileprivate let titleCardLbl: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Halvetica", size: 19)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
 
    fileprivate let descriptionCardLbl: UILabel = {
          let label = UILabel()
          label.font = UIFont(name: "Halvetica", size: 16)
          label.translatesAutoresizingMaskIntoConstraints = false
          return label
      }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareUIs()
    }
    
    fileprivate func prepareUIs() {
        contentView.addSubview(cardImage)
        contentView.addSubview(titleCardLbl)
        contentView.addSubview(descriptionCardLbl)
        let topCardConstraint = cardImage.topAnchor.constraint(equalTo: contentView.topAnchor)
        let centerXCardConstraint = cardImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        let widthCardConstraint = cardImage.widthAnchor.constraint(equalToConstant: contentView.bounds.width)
        let heightCardConstraint = cardImage.heightAnchor.constraint(equalToConstant: contentView.bounds.height * 0.7)
        let titleCenterXCardConstraint = titleCardLbl.centerXAnchor.constraint(equalTo: cardImage.centerXAnchor)
        let titleTopCardConstraint = titleCardLbl.topAnchor.constraint(equalTo: cardImage.bottomAnchor, constant: 10)
        let descriptionTopTitleConstraint = descriptionCardLbl.topAnchor.constraint(equalTo: titleCardLbl.bottomAnchor, constant: 5)
        let desctiptionCenterXConstraint = descriptionCardLbl.centerXAnchor.constraint(equalTo: titleCardLbl.centerXAnchor)
        NSLayoutConstraint.activate([topCardConstraint, centerXCardConstraint, widthCardConstraint, heightCardConstraint, titleCenterXCardConstraint, titleTopCardConstraint, descriptionTopTitleConstraint, desctiptionCenterXConstraint])
    }
    
    func bind(with model: CardModel) {
        Nuke.loadImage(with: URL(string: model.imageAwsUrl)!, into: self.cardImage)
        self.titleCardLbl.text = model.title
        self.descriptionCardLbl.text = model.description
    }
    
    required init?(coder: NSCoder) {
        fatalError("required init not released")
    }
}
