//
//  CardsApi.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation
import Moya


enum CardsApi {
    case getCards
}


extension CardsApi: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://ringtones-kodi.s3.amazonaws.com/data/")!
    }
    
    var path: String {
        return "top_ringtones.json"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var sampleData: Data {
        return APIConstants.defaultSampleData
    }
    
    var headers: [String : String]? {
        return APIConstants.defaultHeaders
    }
}
