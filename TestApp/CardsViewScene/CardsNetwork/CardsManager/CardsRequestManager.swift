//
//  CardsRequestManager.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation
import RxSwift
import Moya


class CardsRequestManager {
    static let shared = CardsRequestManager()
    private let api = MoyaProvider<CardsApi>(plugins: networkLoggerPlugin)
    private init() {}
    
    func getCards() -> Single<[CardModel]> {
        return api.rx.request(.getCards)
        .filterSuccessfulStatusAndRedirectCodes()
        .map([CardModel].self)
    }
}
