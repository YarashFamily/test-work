//
//  CardModel.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation


struct CardModel: Decodable {
    let ringtoneId: String
    let title: String
    let description: String
    let imageAwsUrl: String
    enum CodingKeys: String, CodingKey {
        case ringtoneId = "ringtone_id"
        case title
        case description
        case imageAwsUrl = "image_aws_url"
    }
}
