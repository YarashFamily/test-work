//
//  CardsViewController.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources


class CardsViewController: UIViewController {
    
    var viewModel: CardsViewModel!
    var sceneCoordinator: CardsSceneCoordinator!
    private let infoCard: PublishSubject<String> = PublishSubject<String>()
    
    var collectionView: UICollectionView! = {
        let layout = CenteredCollectionFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width * 0.8, height: UIScreen.main.bounds.height * 0.4)
        layout.minimumLineSpacing = 15
        collectionView.backgroundColor = .white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CardViewCell.self, forCellWithReuseIdentifier: CardViewCell.idCell)
        return collectionView
    }()
    
    var activityIndicator: UIActivityIndicatorView!
    let disposeBag: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cards"
        self.view.backgroundColor = .white
        initViewModel()
        setCollectionConstraints()
    }
    
    private func initViewModel() {
        
        let input = CardsViewModel.UIInputs(loadingEvent: Observable.just(Void()), selectedCardTitle: infoCard.asObserver())
        let outputs = viewModel.transform(inputs: input)
        
        outputs.cards.drive(onNext: {
            print($0)
        })
        .disposed(by: disposeBag)
        
        outputs.cards.drive(collectionView.rx.items(cellIdentifier: CardViewCell.idCell, cellType: CardViewCell.self)) {_,
            model, cell in
            cell.bind(with: model)
        }
        .disposed(by: disposeBag)
        
        collectionView.rx.modelSelected(CardModel.self)
        .flatMapLatest{Observable.just($0.title)}
        .subscribe(onNext: { [unowned self] title in
            self.showAlert(titleImg: title)
        })
        .disposed(by: disposeBag)
        
        collectionView.rx.modelSelected(CardModel.self)
        .flatMapLatest{Observable.just($0.title)}
        .bind(to: infoCard)
        .disposed(by: disposeBag)
        
    }
    
    private func prepareActivityView() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.style = .large
        view.addSubview(activityIndicator)
    }
    
    private func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    private func setCollectionConstraints() {
        view.addSubview(collectionView)
        let centerX = collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let centerY = collectionView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        let widthConstraint = collectionView.widthAnchor.constraint(equalTo: view.widthAnchor)
        let heightConstraint = collectionView.heightAnchor.constraint(equalTo: view.heightAnchor)
        NSLayoutConstraint.activate([centerX, centerY, widthConstraint, heightConstraint])
    }
    
}


extension CardsViewController {
    
    func showAlert(titleImg: String) {
        let vc = UIAlertController(title: "Image Card", message: titleImg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        vc.addAction(alertAction)
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
}
