//
//  CardsViewModel.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CardsViewModel {
    
    var selectedTitle: ReplaySubject<String>
    
    struct UIInputs {
        let loadingEvent: Observable<Void>
        let selectedCardTitle: Observable<String>
    }
    
    struct UIOutputs {
        let cards: Driver<[CardModel]>
    }
    let disposeBag = DisposeBag()
    func transform(inputs: UIInputs) -> UIOutputs {
        let errorTracker = ErrorTracker()
        
        let cards = inputs.loadingEvent.flatMapLatest {
            return CardsRequestManager.shared.getCards()
            .trackError(errorTracker)
        }
        inputs.selectedCardTitle.bind(to: selectedTitle)
            .disposed(by: disposeBag)
        return UIOutputs(cards: cards.asDriverLogError())
    }
    
    init() {
        let subject = ReplaySubject<String>.create(bufferSize: 1)
        selectedTitle = subject.asObserver()
    }
}
