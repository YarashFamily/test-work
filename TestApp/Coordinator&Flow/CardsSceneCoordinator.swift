//
//  CardsSceneCoordinator.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift


class CardsSceneCoordinator {
    weak var parentCoordinator: MainSceneCoordinator!
    var navigationController: UINavigationController!
    var viewModel: CardsViewModel
    
    init(viewModel: CardsViewModel) {
        self.viewModel = viewModel
        let cardVC = CardsViewController()
        cardVC.viewModel = viewModel
        navigationController = UINavigationController(rootViewController: cardVC)
        
    }
}
