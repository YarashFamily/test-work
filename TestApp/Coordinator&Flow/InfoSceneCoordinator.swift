//
//  InfoSceneCoordinator.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift


class InfoSceneCoordinator {
    
    var infoCard = PublishSubject<String>()
    var navigationController: UINavigationController!
    var viewModel: InfoViewModel
    
    init(viewModel: InfoViewModel) {
        self.viewModel = viewModel
        let infoVC = InfoViewController()
        infoVC.sceneCoordinator = self
        infoVC.infoViewModel = viewModel
        navigationController = UINavigationController(rootViewController: infoVC)
    }
    
}
