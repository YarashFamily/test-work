//
//  MainSceneCoordinator.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift

class MainSceneCoordinator {
    
    private var cardSceneCoordinator: CardsSceneCoordinator!
    private var infoSceneCoordinator: InfoSceneCoordinator!
    private var tab3SceneCoordinator: Tab3SceneCoordinator!
    private var tab4SceneCoordinator: Tab4SceneCoordinator!
    private var tab5SceneCoordinator: Tab5SceneCoordinator!
    var parentViewControllers: [UINavigationController]!
    var parentNavigationController: UINavigationController
    let disposeBag = DisposeBag()
    
    init(navigationController: UINavigationController) {
        self.parentNavigationController = navigationController
        self.parentNavigationController.navigationBar.isHidden = true
        let cardsViewModel = CardsViewModel()
        cardSceneCoordinator = CardsSceneCoordinator(viewModel: cardsViewModel)
        cardSceneCoordinator.parentCoordinator = self
        let infoViewModel = InfoViewModel()
        infoSceneCoordinator = InfoSceneCoordinator(viewModel: infoViewModel)
        infoViewModel.selectedInfoTitle = cardsViewModel.selectedTitle.asObservable()
        tab3SceneCoordinator = Tab3SceneCoordinator()
        tab4SceneCoordinator = Tab4SceneCoordinator()
        tab5SceneCoordinator = Tab5SceneCoordinator()
        parentViewControllers = [cardSceneCoordinator.navigationController, infoSceneCoordinator.navigationController, tab3SceneCoordinator.navigationController, tab4SceneCoordinator.navigationController, tab5SceneCoordinator.navigationController]
    }
    
    func runSceneFlow() {
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: "isFirstLaunch")
        if isFirstLaunch {
            showOnbardingScene()
        } else {
            showTabBarController()
        }
    }
    
    private func showOnbardingScene() {
        let onboardingScene = OnboardingViewController()
        onboardingScene.sceneCoordinator = self
        parentNavigationController.interactivePopGestureRecognizer?.isEnabled = false
        parentNavigationController.pushViewController(onboardingScene, animated: true)
    }
    
    private func showTabBarController() {
        let tabbarController = TabBarViewController()
        tabbarController.mainCoordinator = self
        parentNavigationController.interactivePopGestureRecognizer?.isEnabled = false
        parentNavigationController.pushViewController(tabbarController, animated: true)
    }
    
    func runTabBarScene() {
        !UserDefaults.standard.bool(forKey: "isFirstLaunch") ? UserDefaults.standard.set(true, forKey: "isFirstLaunch") : ()
        showTabBarController()
    }
    
}
