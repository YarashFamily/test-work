//
//  Tab3SceneCoordinator.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit


class Tab3SceneCoordinator {
    var navigationController: UINavigationController!
    init() {
        navigationController = UINavigationController(rootViewController: Tab3ViewController())
    }
}
