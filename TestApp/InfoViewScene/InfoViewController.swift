//
//  InfoViewController.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift


class InfoViewController: UIViewController {

    weak var sceneCoordinator: InfoSceneCoordinator!
    var infoViewModel: InfoViewModel!
    var label: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "Halvetica", size: 28)
        label.textColor = .red
        label.text = "Please select card"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationItem.title = "Info"
        setLabelConstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        infoViewModel.selectedInfoTitle.bind(to: label.rx.text)
        .disposed(by: disposeBag)
    }
    private func setLabelConstraint() {
        view.addSubview(label)
        let centerXlabelConstraint = label.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let centerYlabelConstraint = label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        NSLayoutConstraint.activate([centerXlabelConstraint, centerYlabelConstraint])
    }

}
