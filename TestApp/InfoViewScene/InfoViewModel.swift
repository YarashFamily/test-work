//
//  InfoViewModel.swift
//  TestApp
//
//  Created by Andrei on 25.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import Foundation
import RxSwift


class InfoViewModel {

    var selectedInfoTitle: Observable<String>
    
    init() {
        self.selectedInfoTitle = PublishSubject<String>()
    }
    
}
