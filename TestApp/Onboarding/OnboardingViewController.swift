//
//  OnboardingViewController.swift
//  TestApp
//
//  Created by Andrei on 25.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit
import RxSwift


class OnboardingViewController: UIViewController {

    var sceneCoordinator: MainSceneCoordinator!
    
    let button: UIButton = {
        let btn = UIButton(frame: CGRect.zero)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Continue", for: .normal)
        btn.backgroundColor = .green
        return btn
    }()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setBtnConstraints()
        button.rx.tap.subscribe(onNext: { [unowned self] _ in
            self.sceneCoordinator.runTabBarScene()
            
        })
        .disposed(by: disposeBag)
    }
    
    func setBtnConstraints() {
        view.addSubview(button)
        let centerBtnConstraint = button.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let widthBtnConstraint = button.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6)
        let heightBtnConstraint = button.heightAnchor.constraint(equalToConstant: 65)
        let bottomBtnConstraint = button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -25)
        NSLayoutConstraint.activate([centerBtnConstraint, widthBtnConstraint, heightBtnConstraint, bottomBtnConstraint])
    }

}
