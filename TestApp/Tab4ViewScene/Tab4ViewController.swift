//
//  Tab4ViewController.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit

class Tab4ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        navigationItem.title = "Tab 4"
    }
}
