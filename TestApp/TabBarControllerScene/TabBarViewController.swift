//
//  TabBarViewController.swift
//  TestApp
//
//  Created by Andrei on 24.08.2020.
//  Copyright © 2020 AndrewPlayFun. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    weak var mainCoordinator: MainSceneCoordinator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         prepareTabBarController()
    }
    
    private func prepareTabBarController() {
        mainCoordinator.parentViewControllers.enumerated().forEach { (index,vc) in
            switch index {
            case 0:
                vc.tabBarItem = UITabBarItem(title: "Photos", image: #imageLiteral(resourceName: "Photo"), tag: index)
            case 1:
                vc.tabBarItem = UITabBarItem(title: "Info", image: #imageLiteral(resourceName: "Info"), tag: index)
            case 2:
                vc.tabBarItem = UITabBarItem(title: "Tab3", image: #imageLiteral(resourceName: "Tab"), tag: index)
            case 3:
                vc.tabBarItem = UITabBarItem(title: "Tab4", image: #imageLiteral(resourceName: "Tab"), tag: index)
            case 4:
                vc.tabBarItem = UITabBarItem(title: "Tab5", image: #imageLiteral(resourceName: "Tab"), tag: index)
            default: fatalError("Error")
            }
            
        }
        self.viewControllers = mainCoordinator.parentViewControllers
    }

}
